# Zipcodebase

This module integrates Drupal with https://zipcodebase.com/. Zipcodebase is a third-party service that will allow you free access to Worldwide Postal Code Data.

As per the Zipcodebase website:

> Zipcodebase.com is the perfect tool to perform postal code validation, lookups and other calculative tasks, such as postal code distance calculations. Zipcodebase offers a wide range of endpoints that give you access to any type of data you might need.

Read the zipcodebase documentation for more detail.

This is a simple wrapper for the [Zipcodebase API](https://zipcodebase.com/).
This module exposes a Drupal service that can be used to query the API.

## Dependencies

This module uses the `msankhala/zipcodebase-php` library.

## Installation

```bash
composer require drupal/zipcodebase
```

## Uses

This module doesn't provide any functionality on its own. It is meant to be used by other modules.

## Example: Using the ZipCodeBaseService in a controller

```php
<?php

namespace Drupal\zipcodebase\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\zipcodebase\ZipCodeBaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestController controller.
 */
class TestController extends ControllerBase {

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The ZipCodeBaseService.
   *
   * @var \Drupal\zipcodebase\ZipCodeBaseService
   */
  protected $zipCodeBaseService;

  /**
   * TestController constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\zipcodebase\ZipCodeBaseService $zipCodeBaseService
   *   The ZipCodeBaseService.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory, ZipCodeBaseService $zipCodeBaseService) {
    $this->loggerFactory = $loggerFactory;
    $this->zipCodeBaseService = $zipCodeBaseService;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('zipcodebase.zipcodebase')
    );
  }

  /**
   * Test.
   *
   * @return string
   *   Return Hello string.
   */
  public function test() {
    $this->loggerFactory->get('zipcodebase')->notice('TestController::test() called');
    $zipCodeBaseClient = $this->zipCodeBaseService->getClient();
    $credits = $zipCodeBaseClient->getCredits();
    return [
      '#type' => 'markup',
      '#markup' => "Credits: " . print_r($credits, TRUE),
    ];
  }

}
```
