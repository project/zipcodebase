<?php

namespace Drupal\zipcodebase;

use Msankhala\ZipcodebasePhp\ZipcodebaseClient;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class ZipCodeBase service.
 */
class ZipCodeBaseService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ZipCodeBaseService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get the zipcodebase client.
   *
   * @return \Msankhala\ZipcodebasePhp\ZipcodebaseClient
   *   The zipcodebase client.
   */
  public function getClient() {
    $config = $this->configFactory->get('zipcodebase.zipcodebase');
    $api_key = $config->get('api_key');
    $api_url = $config->get('api_url');
    $client = new ZipcodebaseClient($api_key, $api_url);
    return $client;
  }

}
