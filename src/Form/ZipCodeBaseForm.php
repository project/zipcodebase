<?php

namespace Drupal\zipcodebase\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ZipCodeBaseForm config form.
 */
class ZipCodeBaseForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zipcodebase.zipcodebase',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zipcodebase_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the configuration.
    $config = $this->config('zipcodebase.zipcodebase');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Enter the API key for the zipcdoebase. You can get your API key from <a href="https://app.zipcodebase.com" target="_blank">https://app.zipcodebase.com</a>'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#description' => $this->t('Enter the API URL for the zipcdoebase. You can get your API URL from <a href="https://app.zipcodebase.com/documentation" target="_blank">https://app.zipcodebase.com/documentation</a>'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_url') ?? 'https://app.zipcodebase.com/api/v1/',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Configuration'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the API Key.
    if (empty($form_state->getValue('api_key'))) {
      $form_state->setErrorByName('api_key', $this->t('API Key is required.'));
    }
    // Validate the API URL.
    if (empty($form_state->getValue('api_url'))) {
      $form_state->setErrorByName('api_url', $this->t('API URL is required.'));
    }
    else {
      // Validate the API URL.
      $api_url = $form_state->getValue('api_url');
      if (!filter_var($api_url, FILTER_VALIDATE_URL)) {
        $form_state->setErrorByName('api_url', $this->t('API URL is not valid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the configuration.
    $this->config('zipcodebase.zipcodebase')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
